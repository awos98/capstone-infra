# capstone-infra

---

## Description

- 이전에 온프레미스 환경에서 배포를 했었던 [졸업작품 프로젝트](https://gitlab.com/awos98/Capstone-Design)를 AWS 클라우드 내에서 리소스를 사용하여 배포를 하기 위한 인프라 구축 프로젝트
- Terraform으로 AWS 리소스들을 구성하여 자동화 및 휴먼 에러를 줄이고자함
-

## 리소스 공통 목적

- terraform.tfstate 를 s3에 업로드하여 협업시 같은 파일을 공유하기 위해 설정
- 각 리소스들의 정책 및 권한을 iam-group과 iam-user에게 부여

## s3

- s3 구축,

## secrets(secret manager)

- ssm 구축, 민감한 정보들을 다른 리소스에서 사용하기 위해 설정

## vpc

- vpc(`pub subnets(2개, a, c), pvt subnets(2개, a, c), routes table, nat gateway, internet gateway, default sg 등`)의 리소스들을 구축

## ec2

- jenkins server 구축, eks 노드들에 접근할 수 있는 kubectl 명령어를 사용하게끔 설정

## eks

- eks 구축, `min=2, max=4, desired=2` 로 설정
